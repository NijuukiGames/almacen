using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[RequireComponent(typeof(RawImage))]
public class EfectoCortina : MonoBehaviour
{
    private Material dissolve;
    [SerializeField]
    private Texture Cortina;

    void Start()
    {
        dissolve = GetComponent<RawImage>().material;

        dissolve.SetTexture(Shader.PropertyToID("Texture2D_93A3B42"),  Cortina);
    }

    public void setCutOff(float value)
    {
        dissolve.SetFloat(Shader.PropertyToID("_CutoffHeight"), value);
    }
}
